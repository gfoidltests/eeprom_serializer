﻿/*
	Reminder: By templates the compiler has to have access to the full code in order to generate the proper classes (at compile-time).
	Therefore the definition and declaration has to be accessible. Easiest way is all in one header file.
 */

#ifndef _EEPROM_SerializerLib_h
#define _EEPROM_SerializerLib_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif

#include <EEPROM.h>
//-----------------------------------------------------------------------------
template <typename T>
class EepromSerializer
{
private:
	unsigned int _offsetFromAddress0;
	EepromSerializer();

public:
	EepromSerializer(unsigned int offsetFromAddress0);

	void Write(const T& object, const bool onlyWriteNewValue = true);
	T* Read();
};
//-----------------------------------------------------------------------------
template <typename T>
EepromSerializer<T>::EepromSerializer(unsigned int offsetFromAddress0) : _offsetFromAddress0(offsetFromAddress0) {}
//-----------------------------------------------------------------------------
template <typename T>
void EepromSerializer<T>::Write(const T& object, const bool onlyWriteNewValue)
{
	// overlay the pointer to the storage onto the buffer -> this is amazing in C/C++
	byte* buffer = (byte*)&object;

	for (int i = 0; i < sizeof(T); ++i)
	{
		int addr = i + _offsetFromAddress0;

		if (onlyWriteNewValue)
			EEPROM.update(addr, buffer[i]);
		else
			EEPROM.write(addr, buffer[i]);
	}
}
//-----------------------------------------------------------------------------
template <typename T>
T* EepromSerializer<T>::Read()
{
	byte buffer[sizeof(T)];

	for (int i = 0; i < sizeof(T); ++i)
		buffer[i] = EEPROM.read(i + _offsetFromAddress0);

	// buffer is local and will be destroyed at end of function -> so create a new reference
	// a good compiler will optimize this copying
	T* ptr = (T*)buffer;
	return new T(*ptr);
}

#endif